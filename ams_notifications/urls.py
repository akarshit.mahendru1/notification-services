"""ams_notifications URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from fcm_django.api.rest_framework import FCMDeviceViewSet
from django.conf.urls import url,include
from rest_framework_swagger.views import get_swagger_view
#from rest_framework_swagger.renderers import SwaggerUIRenderer, OpenAPIRenderer

schema_view = get_swagger_view(title='Notification Services')
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('notifications.urls')),
    url(r'docs/', schema_view),
    url(r'^fcm-devices/$', FCMDeviceViewSet.as_view({'post': 'create'}), name='create_fcm_device')
]
