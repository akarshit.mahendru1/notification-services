from django.template import loader
from django.conf import settings

from twilio.rest import Client
from .exceptions import TwilloMessageSendException

client = Client(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)
print(settings.TWILIO_ACCOUNT_SID)

class SendMessage(object):
    msg_template_name = None

    def send_message(self, to, **ctx):
        plain_body = loader.render_to_string(self.msg_template_name, ctx)
        try:
            message = client.messages.create(
                body=plain_body,
                to=to,
                from_=settings.FROM_NUMBER
            )
        except Exception as ex:
            raise TwilloMessageSendException(str(ex))