from celery import Task
from fcm_django.models import FCMDevice


class SendPushNotification(Task):
    """ send the push notification to user """


    def run(self, user, title, message, extra_data, *args, **kwargs):
        """

        :param user: user id to send them notifications
        :param title: tilte of push notification message
        :param message: push notification body content
        :param extra_data: extra that with push message
        :return:
        """


        ### get the device of user

        if user == "__all__":
            devices = FCMDevice.objects.all()
        else:
            devices = FCMDevice.objects.filter()

        for device in devices:
            device.send_message(title=title, body=message, data=extra_data)
