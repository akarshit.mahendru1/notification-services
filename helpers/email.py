from django.core.mail import EmailMessage
from django.template import loader
from django.conf import settings


class EmailMixin(object):
    name = None
    subject_template_name = None
    html_body_template_name = None
    cc = []

    def create_email(self, **ctx):
        subject = loader.render_to_string(self.subject_template_name, ctx)
        subject = ''.join(subject.splitlines())
        html_body = loader.render_to_string(self.html_body_template_name, ctx)
        cc = ctx['cc'] if ctx.get('cc') else []

        email_message = EmailMessage(
            subject,
            html_body,
            settings.DEFAULT_FROM_EMAIL,
            [ctx['user_email']],
            cc=cc
        )

        email_message.content_subtype = 'html'
        email_message.send(fail_silently=True)
