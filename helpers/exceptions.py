class TwilloMessageSendException(Exception):
    def __init__(self, msg):
        # Call the base class constructor with the parameters it needs
        super(TwilloMessageSendException, self).__init__(msg)
