from celery import Task
from ams_notifications import celery_app

from helpers.email import EmailMixin
from helpers.msgs import SendMessage


class SendNotificationOnEmail(Task, EmailMixin):
    """sending E-MAIL NOTIFICATION to the respective user's registered e-mail"""

    name = "AMS_Notification"
    subject_template_name = None
    html_body_template_name = None

    def run(self, **ctx):

        self.subject_template_name = ctx.get('subject_template_name', None)
        self.html_body_template_name = ctx.get('html_body_template_name', None)

        self.create_email(**ctx)


celery_app.tasks.register(SendNotificationOnEmail())


class SendNotificationOnSMS(Task,SendMessage):
    """sending SMS NOTIFICATION to the respective user's registered mobile number"""

    msg_template_name = None

    def run(self, **ctx):

        self.msg_template_name = ctx.get('msg_template_name', None)
        self.send_message(**ctx)

# celery_app.tasks.register(SendNotificationOnSMS())



