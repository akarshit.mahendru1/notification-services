from django.conf.urls import include, url
from rest_framework import routers

from .views import *


router = routers.DefaultRouter()
router.register('templates',TemplateViewSet,base_name='template-upload')


urlpatterns = [
   url(r'^', include(router.urls)),
   url(r'^generate-notifications/$', NotificationViewSet.as_view())
]