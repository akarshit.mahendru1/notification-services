from rest_framework import serializers,fields
from . models import Notification,TemplateUpload
from .models import MODE

class TemplateSerializer(serializers.ModelSerializer):

    """ serializer for templates """
    content=serializers.CharField(style={'base_template':'input.html'})

    class Meta:
        model = TemplateUpload
        fields =('id','name','content')

class NotificationSerializer(serializers.ModelSerializer):

    """ serializer for notification """
    notification_mode=fields.MultipleChoiceField(choices=MODE,allow_null=True,required=False)

    class Meta:
        model = Notification
        fields = "__all__"