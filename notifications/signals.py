from django.dispatch import Signal, receiver
from helpers.pushnotifications import SendPushNotification

send_push_notification = Signal(providing_args=["user","title", "message"])

@receiver(send_push_notification)
def send_push_notification_receiver(sender, **kwargs):
    """

    :param sender:
    :param kwargs:
    :return:  send the push notifications to the user
    """

    push_notification_obj = SendPushNotification()

    push_notification_obj.run(**kwargs)





