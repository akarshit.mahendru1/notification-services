from django.db import models
import uuid
from django.contrib.postgres.fields import ArrayField
from django.utils.translation import gettext_lazy as _
# Create your models here.


class TemplateUpload(models.Model):
    name=models.CharField(max_length=50)
    content=models.TextField()

    def __str__(self):
        return "%s" % (self.name)


EMAIL = "email"
SMS = "sms"
PUSH = "push"
IN_APP="in_app"
MODE = (
    (EMAIL, _("email")),
    (SMS, _("sms")),
    (PUSH, _("push")),
    (IN_APP,_("in_app"))
)

class Notification(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    recipient=ArrayField(models.CharField(max_length=200))
    notification_mode=models.CharField(max_length=200,null=True,blank=True,choices=MODE)  #Could be e-mail,sms,in app or push notifications
    template_name=models.ForeignKey(TemplateUpload, related_name='notification', on_delete=models.CASCADE, null=True)
    message = models.CharField(max_length=100)

    def __str__(self):
        return self.message

