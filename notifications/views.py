from django.shortcuts import render
from . models import Notification,TemplateUpload
from . serializers import NotificationSerializer,TemplateSerializer
from rest_framework import viewsets,generics,response,exceptions,status
from . tasks import SendNotificationOnEmail,SendNotificationOnSMS
from .signals import send_push_notification
from django.db import transaction
import re
# Create your views here.
#Template CRUD

class TemplateViewSet(viewsets.ModelViewSet):
    model= TemplateUpload
    serializer_class = TemplateSerializer

    def get_queryset(self):
        data = self.model.objects.all()
        return data



#Notifications:CREATE
class NotificationViewSet(generics.ListAPIView,generics.CreateAPIView):
    model=Notification
    serializer_class = NotificationSerializer
    queryset = Notification.objects.all()

    def perform_create(self, serializer):
        with transaction.atomic():
            notification=serializer.save()
            message=serializer.validated_data['message']
            recipient=serializer.validated_data['recipient']

        email=[]
        mobile=[]

        for rec in recipient:
            if '@' in rec:
                serializer.validated_data['notification_mode']="email"
                email.append(rec)

            elif re.search('^\+[1-9]{1,3}[0-9]{3,14}$',rec):
                serializer.validated_data['notification_mode'] = "sms"
                mobile.append(rec)

        serializer.save()
        send_push_notification.send(
            self,
            user=recipient,
            title='AMS_NOTIFICATION',
            message=message,
            extra_data = {'screen_name': "HOME_SCREEN"})
        try:

            if serializer.validated_data['notification_mode']=="email":
                for mail in email:
                    ctx = {
                        'subject_template_name': 'ams_notifications',
                        'user_email': str(mail),
                        'html_body_template_name': 'message',
                        'message':message
                    }
                    try:
                        task=SendNotificationOnEmail()
                        task.run(**ctx)
                    except Exception as e:
                        print(e)

            elif serializer.validated_data['notification_mode']=="sms":
                for number in mobile:
                    ctx = {
                        'msg_template_name': 'message',
                        'to': str(number),
                        'message': message
                    }
                    try:
                        task=SendNotificationOnSMS()
                        task.run(**ctx)
                    except Exception as e:
                        print(e)

        except Exception as e:
            raise exceptions.ValidationError("Enter valid E-mail or mobile number")

        return response.Response(notification.id,status=status.HTTP_200_OK)

